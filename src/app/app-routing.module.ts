import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePageComponent } from 'src/app/pages/home-page/home-page.component';
import { CaseStudyPageComponent } from 'src/app/pages/case-study-page/case-study-page.component';
import { JobsInternshipsPageComponent } from 'src/app/pages/jobs-internships-page/jobs-internships-page.component';
import { NewsPageComponent } from 'src/app/pages/news-page/news-page.component';
import { PartnersPageComponent } from 'src/app/pages/partners-page/partners-page.component';
import { SamplePageComponent } from 'src/app/pages/sample-page/sample-page.component';
import { SamplePageTwoComponent } from 'src/app/pages/sample-page-two/sample-page-two.component';



const routes: Routes = [
	{ path: '', component: HomePageComponent },
	{ path: 'case-study', component: CaseStudyPageComponent },
	{ path: 'jobs-and-internships', component: JobsInternshipsPageComponent },
	{ path: 'news', component: NewsPageComponent },
	{ path: 'partners', component: PartnersPageComponent },
	{ path: 'sample-page', component: SamplePageComponent },
	{ path: 'sample-page-two', component: SamplePageTwoComponent }
];




@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdgButtonModule } from 'src/app/shared-modules/ad-garlic-ui/ad-garlic-ui.module';

import { JobsInternshipsSectionComponent } from './jobs-internships-section.component';
import { SectionBannerComponent } from '../section-banner/section-banner.component';

describe('JobsInternshipsSectionComponent', () => {
	let component: JobsInternshipsSectionComponent;
	let fixture: ComponentFixture<JobsInternshipsSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				JobsInternshipsSectionComponent,
				SectionBannerComponent
			],
			imports: [
				AdgButtonModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JobsInternshipsSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

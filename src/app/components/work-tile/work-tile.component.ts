import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/project';

@Component({
	selector: 'app-work-tile',
	templateUrl: './work-tile.component.html',
	styleUrls: ['./work-tile.component.scss']
})
export class WorkTileComponent implements OnInit {
	
	@Input() project: Project;
	@Output() imageLoaded = new EventEmitter<any>();
	isHovered: boolean = false;
	
	

	ngOnInit() {
	}




	redirectToProject(project: Project): void {
		if(project.title != '') {
			alert(project.title);
		}
	}




	onImageLoaded(): void {
		this.imageLoaded.emit();
	}
	




}

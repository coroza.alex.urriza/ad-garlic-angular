import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-main-footer',
	templateUrl: './main-footer.component.html',
	styleUrls: ['./main-footer.component.scss']
})
export class MainFooterComponent implements OnInit {

	ngOnInit() {
	}




	socialShare(socialMedia: 'facebook'|'twitter'|'google-plus') : void {
		let url = encodeURIComponent('https://adgarlic.com');
		
		if(socialMedia == 'facebook') {
			window.open("https://www.facebook.com/sharer/sharer.php?u=" + url);
		} else if (socialMedia == 'google-plus') {
			window.open("https://plus.google.com/share?url=" + url);
		} else if(socialMedia == 'twitter') {
			window.open("https://twitter.com/home?status=" + url);
		}
	}
	
	
	
	
}

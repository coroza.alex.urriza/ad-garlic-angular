import { Component, OnInit, Input } from '@angular/core';
import { News } from 'src/app/models/news';

@Component({
	selector: 'app-news-summary',
	templateUrl: './news-summary.component.html',
	styleUrls: ['./news-summary.component.scss']
})
export class NewsSummaryComponent implements OnInit {

	@Input() news: News;
	

	ngOnInit() {
	}

}

import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-section-banner',
	templateUrl: './section-banner.component.html',
	styleUrls: ['./section-banner.component.scss']
})
export class SectionBannerComponent implements OnInit {

	@Input() imageUrl: string = '';
	@Input() label: string = '';
	

	ngOnInit() {
	}

}

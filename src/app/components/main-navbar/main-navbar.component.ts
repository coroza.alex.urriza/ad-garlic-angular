import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NavigationItem } from 'src/app/models/navigation-item';



@Component({
	selector: 'app-main-navbar',
	templateUrl: './main-navbar.component.html',
	styleUrls: ['./main-navbar.component.scss']
})



export class MainNavbarComponent implements OnInit {

	@Input() displayedNavItemsIndex: number[] = [];
	@Input() navigationItems: NavigationItem[] = [];
	@Output() sidebarNavToggled = new EventEmitter<any>();
	@Output() navigationItemClicked = new EventEmitter<NavigationItem>();
	
	

	ngOnInit() {
	}




	toggleSidebarNav() : void {
		this.sidebarNavToggled.emit();
	}




	navigateTo(navigationItem: NavigationItem) : void {
		this.navigationItemClicked.emit(navigationItem);
	}
	
	
	

}

import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { GalleryItem } from 'src/app/models/gallery-item';

import { GalleryItemPreviewModalComponent } from '../gallery-item-preview-modal/gallery-item-preview-modal.component';


@Component({
	selector: 'app-originals-section',
	templateUrl: './originals-section.component.html',
	styleUrls: ['./originals-section.component.scss']
})
export class OriginalsSectionComponent implements OnInit {

	galleryImages: NgxGalleryImage[];
	galleryOptions: NgxGalleryOptions[];
	isSectionVisible: boolean;




	constructor(
		private ngbModalService: NgbModal
	) { }




	ngOnInit() {
		this.galleryImages = [
			{ small: 'assets/images/originals-image-1.jpg', medium: 'assets/images/originals-image-1.jpg', big: 'assets/images/originals-image-1.jpg' },
			{ small: 'assets/images/originals-image-2.jpg', medium: 'assets/images/originals-image-2.jpg', big: 'assets/images/originals-image-2.jpg' },
			{ small: 'assets/images/news-3.jpg', medium: 'assets/images/news-3.jpg', big: 'assets/images/news-3.jpg' },
		];

		this.galleryOptions = [
			{
				image: false,
				preview: false,
				width: '100%',
				height: '250px',
				thumbnailsColumns: 2,
				thumbnailsRows: 1,
				thumbnailsSwipe: true,
				arrowPrevIcon: 'mdi mdi-chevron-left-circle-outline',
				arrowNextIcon: 'mdi mdi-chevron-right-circle-outline',
				closeIcon: 'mdi mdi-close',
				previewCloseOnEsc: true,
			}
		];
	}




	ngxGalleryClicked(event: any): void {
		this.openGalleryPreview(event.image);
	}




	openGalleryPreview(galleryImage: NgxGalleryImage): void {
		let galleryItem: GalleryItem = {
			sourceUrl: galleryImage.small,
			itemType: 'image'
		};

		const modalRef = this.ngbModalService.open(GalleryItemPreviewModalComponent, {
			centered: true
		});
		modalRef.componentInstance.galleryItem = galleryItem;
	}




}

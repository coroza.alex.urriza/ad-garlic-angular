import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgxGalleryModule } from 'ngx-gallery';

import { OriginalsSectionComponent } from './originals-section.component';


describe('OriginalsSectionComponent', () => {
	let component: OriginalsSectionComponent;
	let fixture: ComponentFixture<OriginalsSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				OriginalsSectionComponent
			],
			imports: [
				NgxGalleryModule
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(OriginalsSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

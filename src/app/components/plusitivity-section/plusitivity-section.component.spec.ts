import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PlusitivitySectionComponent } from './plusitivity-section.component';





describe('PlusitivitySectionComponent', () => {
	let component: PlusitivitySectionComponent;
	let fixture: ComponentFixture<PlusitivitySectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				PlusitivitySectionComponent
			],
			imports: [
				NgbModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PlusitivitySectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

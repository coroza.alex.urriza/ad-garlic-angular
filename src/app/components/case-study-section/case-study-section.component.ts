import { Component, OnInit } from '@angular/core';
import { YoutubeEmbedItem } from 'src/app/shared-modules/youtube-embed/models/youtube-embed-item';

@Component({
	selector: 'app-case-study-section',
	templateUrl: './case-study-section.component.html',
	styleUrls: ['./case-study-section.component.scss']
})
export class CaseStudySectionComponent implements OnInit {


	isSectionVisible: boolean;
	
	youtubeVideos : YoutubeEmbedItem[] = [
		{ thumbnailSrc: 'assets/images/case-study-youtube-embed-preview-1.jpg', videoSrc: 'https://www.youtube.com/embed/-QU-yuchw3E?autoplay=1&controls=0' },
		{ thumbnailSrc: 'assets/images/case-study-youtube-embed-preview-2.jpg', videoSrc: 'https://www.youtube.com/embed/Uz0DV6B3Eno?autoplay=1&controls=0' },
		{ thumbnailSrc: 'assets/images/case-study-youtube-embed-preview-3.jpg', videoSrc: 'https://www.youtube.com/embed/AmxFJO5c8xw?autoplay=1&controls=0' }
	];
	
	
	

	ngOnInit() {
	}

}

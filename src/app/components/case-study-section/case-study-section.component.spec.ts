import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { CaseStudySectionComponent } from './case-study-section.component';
import { YoutubeEmbedComponent } from 'src/app/shared-modules/youtube-embed/components/youtube-embed/youtube-embed.component';


describe('CaseStudySectionComponent', () => {
	let component: CaseStudySectionComponent;
	let fixture: ComponentFixture<CaseStudySectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				CaseStudySectionComponent,
				YoutubeEmbedComponent,
			],
			imports: [
				NgbModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CaseStudySectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

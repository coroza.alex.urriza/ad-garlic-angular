import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargedPeepsSectionComponent } from './charged-peeps-section.component';

describe('ChargedPeepsSectionComponent', () => {
	let component: ChargedPeepsSectionComponent;
	let fixture: ComponentFixture<ChargedPeepsSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ChargedPeepsSectionComponent]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChargedPeepsSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

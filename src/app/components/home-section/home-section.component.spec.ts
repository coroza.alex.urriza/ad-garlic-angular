import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubeEmbedModule } from 'src/app/shared-modules/youtube-embed/youtube-embed.module';

import { HomeSectionComponent } from './home-section.component';





describe('HomeSectionComponent', () => {
	let component: HomeSectionComponent;
	let fixture: ComponentFixture<HomeSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				HomeSectionComponent,
			],
			imports: [
				YoutubeEmbedModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HomeSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

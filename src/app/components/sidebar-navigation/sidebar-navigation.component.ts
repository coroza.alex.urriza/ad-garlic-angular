import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NavigationItem } from 'src/app/models/navigation-item';

@Component({
	selector: 'app-sidebar-navigation',
	templateUrl: './sidebar-navigation.component.html',
	styleUrls: ['./sidebar-navigation.component.scss']
})
export class SidebarNavigationComponent implements OnInit {

	@Input() navigationItems: NavigationItem[] = [];
	@Output() navigationItemClicked = new EventEmitter<NavigationItem>();
	
	
	
	constructor() { }




	ngOnInit() {
	}




	navigateTo(navigationItem: NavigationItem) : void {
		this.navigationItemClicked.emit(navigationItem);
	}
	
	
	

}

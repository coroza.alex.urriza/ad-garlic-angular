import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGalleryModule } from 'ngx-gallery';
import { YoutubeEmbedModule } from 'src/app/shared-modules/youtube-embed/youtube-embed.module';

import { PartnersSectionComponent } from './partners-section.component';
import { SectionBannerComponent } from '../section-banner/section-banner.component';



describe('PartnersSectionComponent', () => {
	let component: PartnersSectionComponent;
	let fixture: ComponentFixture<PartnersSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				PartnersSectionComponent,
				SectionBannerComponent,
			],
			imports: [
				NgxGalleryModule,
				YoutubeEmbedModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PartnersSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

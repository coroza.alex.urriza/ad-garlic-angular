import { Component, OnInit } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage } from 'ngx-gallery';




@Component({
	selector: 'app-partners-section',
	templateUrl: './partners-section.component.html',
	styleUrls: ['./partners-section.component.scss']
})
export class PartnersSectionComponent implements OnInit {

	galleryImages: NgxGalleryImage[];
	galleryOptions: NgxGalleryOptions[];
	isSectionVisible: boolean;


	

	ngOnInit() {
		this.galleryOptions = [
			{ image: false, width: '100%', height: '150px', thumbnailsColumns: 3, arrowPrevIcon: '', arrowNextIcon: '', closeIcon: 'mdi mdi-close' },
		];
		
		this.galleryImages = [
			{ small: 'assets/images/work-advanced.jpg', medium: 'assets/images/work-advanced.jpg', big: 'assets/images/work-advanced.jpg' },
			{ small: 'assets/images/work-vernel.jpg', medium: 'assets/images/work-vernel.jpg', big: 'assets/images/work-vernel.jpg' },
			{ small: 'assets/images/work-azkals.jpg', medium: 'assets/images/work-azkals.jpg', big: 'assets/images/work-azkals.jpg' },
			{ small: 'assets/images/work-betadine.jpg', medium: 'assets/images/work-betadine.jpg', big: 'assets/images/work-betadine.jpg' },
		];
	}


	

}

import { Component, OnInit } from '@angular/core';
import { AdGarlicService } from 'src/app/models/ad-garlic-service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';

@Component({
	selector: 'app-services-section',
	templateUrl: './services-section.component.html',
	styleUrls: ['./services-section.component.scss']
})
export class ServicesSectionComponent implements OnInit {


	galleryImages: NgxGalleryImage[];
	galleryOptions: NgxGalleryOptions[];
	isSectionVisible: boolean;
	selectedService: AdGarlicService;
	serviceDetailsView: boolean;
	services: AdGarlicService[];
	
	
	
	
	constructor(
	) { }





	ngOnInit() {
		this.galleryImages = [];
		
		this.galleryOptions = [
			{ image: false, width: '100%', height: '150px', thumbnailsColumns: 3, arrowPrevIcon: '', arrowNextIcon: '', closeIcon: 'mdi mdi-close' },
		];
		
		this.selectedService = null;
		
		this.serviceDetailsView = false;
		
		this.services = [
			{ name: 'atl', fullName: 'Above The Line', icon: 'mdi-monitor', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'digital', fullName: 'Digital', icon: 'mdi-laptop', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'brand consultancy', fullName: 'Brand Consultancy', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], icon: 'mdi-account-multiple-outline', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'public relations', fullName: 'Public Relations', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], icon: 'mdi-bullhorn-outline', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'activations', fullName: 'Activations', icon: 'mdi-power-standby', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'events', fullName: 'Events', icon: 'mdi-calendar-alert', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'cityscaping', fullName: 'Cityscaping', icon: 'mdi-city-variant-outline', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ name: 'content development and marketing', fullName: 'Content Development and Marketing', icon: 'mdi-file-document-outline', galleryItems:['assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg', 'assets/images/home-section-image.jpg'], description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' }
		];
	}




	viewServiceDetails(clickedService: AdGarlicService) : void {
		this.selectedService = clickedService;
		this.serviceDetailsView = true;
	}




	generateGalleryImages(clickedService: AdGarlicService): void {
		clickedService.galleryItems.forEach((galleryItem) => {
			this.galleryImages.push({
				small: galleryItem,
				medium: galleryItem,
				big: galleryItem
			});
		});
	}




	backToServicesListView() : void {
		this.serviceDetailsView = false;
		this.galleryImages = [];
		this.selectedService = null;
	}


	
	
}

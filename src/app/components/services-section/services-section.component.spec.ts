import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxGalleryModule } from 'ngx-gallery';
import { AdgButtonModule } from 'src/app/shared-modules/ad-garlic-ui/ad-garlic-ui.module';

import { ServicesSectionComponent } from './services-section.component';





describe('ServicesSectionComponent', () => {
	let component: ServicesSectionComponent;
	let fixture: ComponentFixture<ServicesSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				ServicesSectionComponent
			],
			imports: [
				NgxGalleryModule,
				AdgButtonModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ServicesSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

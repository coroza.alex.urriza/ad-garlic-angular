import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsSectionComponent } from './news-section.component';
import { SectionBannerComponent } from '../section-banner/section-banner.component';
import { NewsSummaryComponent } from '../news-summary/news-summary.component';




describe('NewsSectionComponent', () => {
	let component: NewsSectionComponent;
	let fixture: ComponentFixture<NewsSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				NewsSectionComponent,
				SectionBannerComponent,
				NewsSummaryComponent
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NewsSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

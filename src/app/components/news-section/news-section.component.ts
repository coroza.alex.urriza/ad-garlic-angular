import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/news';

@Component({
	selector: 'app-news-section',
	templateUrl: './news-section.component.html',
	styleUrls: ['./news-section.component.scss']
})
export class NewsSectionComponent implements OnInit {

	
	isSectionVisible: boolean;
	newsDetailsView: boolean;
	newsList: News[];
	selectedNews: News;
	
	

	ngOnInit() {
		this.newsDetailsView = false;

		this.newsList = [
			{ title: 'Insert Blog Title Here', bannerImageUrl:'assets/images/news-1.jpg', tags: ['Adverstisements', 'Smart Quotes', 'Unique Design'], textContent: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ title: 'Insert Blog Title Here', bannerImageUrl:'assets/images/news-2.jpg', tags: ['Adverstisements', 'Smart Quotes', 'Unique Design'], textContent: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
			{ title: 'Insert Blog Title Here', bannerImageUrl:'assets/images/news-3.jpg', tags: ['Adverstisements', 'Smart Quotes', 'Unique Design'], textContent: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' },
		];
	}




	selectNews(clickedNews: News): void {
		this.selectedNews = clickedNews;
		this.newsDetailsView = true;
	}
	
	
	
	

}

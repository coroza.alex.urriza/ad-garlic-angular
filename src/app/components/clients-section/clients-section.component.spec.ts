import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMasonryModule } from 'ngx-masonry';

import { ClientsSectionComponent } from './clients-section.component';


describe('ClientsSectionComponent', () => {
	let component: ClientsSectionComponent;
	let fixture: ComponentFixture<ClientsSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				ClientsSectionComponent,
			],
			imports: [
				NgxMasonryModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ClientsSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

import { Component, OnInit } from '@angular/core';
import { NgxMasonryOptions } from 'ngx-masonry';

import { Client } from 'src/app/models/client';

@Component({
	selector: 'app-clients-section',
	templateUrl: './clients-section.component.html',
	styleUrls: ['./clients-section.component.scss']
})



export class ClientsSectionComponent implements OnInit {

	clients: Client[] = [
		{ name: 'Lysol', logoUrl: 'assets/images/clients-lysol.png' },
		{ name: 'Tanduay Select', logoUrl: 'assets/images/clients-tanduay-select.png' },
		{ name: 'Tanduay Rum', logoUrl: 'assets/images/clients-tanduay-rum.png' },
		{ name: 'Nivea Men', logoUrl: 'assets/images/clients-nivea-men.png' },
		{ name: 'WeChat', logoUrl: 'assets/images/clients-wechat.png' },
		{ name: 'Durex', logoUrl: 'assets/images/clients-durex.png' },
		{ name: 'Vernel', logoUrl: 'assets/images/clients-vernel.png' },
		{ name: 'Betadine', logoUrl: 'assets/images/clients-betadine.png' },
		{ name: 'Conrad Manila', logoUrl: 'assets/images/clients-conrad.png' },
		{ name: 'Globe', logoUrl: 'assets/images/clients-globe.png' },
		{ name: 'Sunkist', logoUrl: 'assets/images/clients-sunkist.png' },
		{ name: 'Strepsils', logoUrl: 'assets/images/clients-strepsils.png' }
	];

	isSectionVisible: boolean;

	masonryOptions: NgxMasonryOptions = {
		fitWidth: true,
		horizontalOrder: true,
		transitionDuration: '0'
	};

	fullClientListShown: boolean = false;
	
	
	
	constructor() { }
	
	

	ngOnInit() {
	}

}

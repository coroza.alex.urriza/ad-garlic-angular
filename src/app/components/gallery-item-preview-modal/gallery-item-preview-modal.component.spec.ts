import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { GalleryItemPreviewModalComponent } from './gallery-item-preview-modal.component';

describe('GalleryItemPreviewModalComponent', () => {
	let component: GalleryItemPreviewModalComponent;
	let fixture: ComponentFixture<GalleryItemPreviewModalComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				GalleryItemPreviewModalComponent
			],
			imports: [
				NgbModule
			],
			providers: [
				NgbActiveModal
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(GalleryItemPreviewModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

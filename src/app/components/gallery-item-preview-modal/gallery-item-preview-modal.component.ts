import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { GalleryItem } from 'src/app/models/gallery-item';



@Component({
	selector: 'app-gallery-item-preview-modal',
	templateUrl: './gallery-item-preview-modal.component.html',
	styleUrls: ['./gallery-item-preview-modal.component.scss']
})
export class GalleryItemPreviewModalComponent implements OnInit {

	@Input() galleryItem: GalleryItem;
	
	
	
	constructor(
		public activeModal: NgbActiveModal
	) { }


	

	ngOnInit() {
	}


	
	
}

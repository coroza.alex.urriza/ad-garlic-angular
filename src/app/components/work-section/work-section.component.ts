import { Component, OnInit } from '@angular/core';
import { NgxMasonryOptions } from 'ngx-masonry';

import { Project } from 'src/app/models/project';



@Component({
	selector: 'app-work-section',
	templateUrl: './work-section.component.html',
	styleUrls: ['./work-section.component.scss']
})



export class WorkSectionComponent implements OnInit {

	
	isSectionVisible: boolean;
	
	masonryOptions: NgxMasonryOptions = {
		gutter: 5,
		fitWidth: true,
		horizontalOrder: true,
		transitionDuration: '0'
	};
	
	projects: Project[] = [
		{ id: 1, title: 'tanduay select', imageUrl: 'assets/images/work-tanduay.jpg' },
		{ id: 2, title: 'nivea men', imageUrl: 'assets/images/work-nivea.jpg' },
		{ id: 3, title: '', imageUrl: 'assets/images/work-blank-1.jpg' },
		{ id: 4, title: 'vernel', imageUrl: 'assets/images/work-vernel.jpg' },
		{ id: 5, title: 'advanced', imageUrl: 'assets/images/work-advanced.jpg' },
		{ id: 7, title: 'azkals', imageUrl: 'assets/images/work-azkals.jpg' },
		{ id: 8, title: 'betadine', imageUrl: 'assets/images/work-betadine.jpg' },
		{ id: 6, title: '', imageUrl: 'assets/images/work-blank-2.jpg' },
		{ id: 9, title: 'durex', imageUrl: 'assets/images/work-durex.jpg' },
	];

	updateMasonryLayout: boolean = false;
	
	
	
	ngOnInit() {
	}






}

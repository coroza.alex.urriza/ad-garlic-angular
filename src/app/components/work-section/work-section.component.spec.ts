import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMasonryModule } from 'ngx-masonry';

import { WorkSectionComponent } from './work-section.component';
import { WorkTileComponent } from '../work-tile/work-tile.component';





describe('WorkSectionComponent', () => {
	let component: WorkSectionComponent;
	let fixture: ComponentFixture<WorkSectionComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				WorkSectionComponent,
				WorkTileComponent
			],
			imports: [
				NgxMasonryModule
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WorkSectionComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

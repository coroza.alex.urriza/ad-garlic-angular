import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdgButtonComponent } from './adg-button.component';

describe('AdgButtonComponent', () => {
  let component: AdgButtonComponent;
  let fixture: ComponentFixture<AdgButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdgButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdgButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'adg-button',
	templateUrl: './adg-button.component.html',
	styleUrls: ['./adg-button.component.scss']
})
export class AdgButtonComponent implements OnInit {

	@Input() size: string = 'medium';
	

	ngOnInit() {
	}

}

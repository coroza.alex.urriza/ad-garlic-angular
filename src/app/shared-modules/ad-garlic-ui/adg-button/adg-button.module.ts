import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdgButtonComponent } from './components/adg-button/adg-button.component';



@NgModule({
	declarations: [
		AdgButtonComponent
	],
	imports: [
		CommonModule
	],
	exports: [
		AdgButtonComponent
	]
})
export class AdgButtonModule { }

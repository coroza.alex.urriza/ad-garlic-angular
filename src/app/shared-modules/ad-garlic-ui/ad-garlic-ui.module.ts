import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

export { AdgButtonModule } from './adg-button/adg-button.module';



@NgModule({
	declarations: [],
	imports: [
		CommonModule,
	],
	exports: []
})
export class AdGarlicUiModule { }

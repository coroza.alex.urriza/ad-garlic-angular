// It also depends on mdi icons

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { YoutubeEmbedComponent } from './components/youtube-embed/youtube-embed.component';
import { YteCustomOverlayComponent } from './components/yte-custom-overlay/yte-custom-overlay.component';

@NgModule({
	declarations: [
		YoutubeEmbedComponent,
		YteCustomOverlayComponent
	],
	imports: [
		CommonModule,
		FlexLayoutModule,
		LazyLoadImageModule
	],
	exports: [
		YoutubeEmbedComponent,
		YteCustomOverlayComponent
	],
})
export class YoutubeEmbedModule { }

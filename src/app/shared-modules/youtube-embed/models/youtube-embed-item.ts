export interface YoutubeEmbedItem {
	videoSrc: string;
	thumbnailSrc: string;
}
import { Component, OnInit, Input, Output } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
	selector: 'youtube-embed',
	templateUrl: './youtube-embed.component.html',
	styleUrls: ['./youtube-embed.component.scss']
})



export class YoutubeEmbedComponent implements OnInit {

	@Input() backgroundColor: string = '#212121';
	@Input() customOverlay: boolean = false;
	@Input() thumbnailSrc: string;	
	@Input() videoSrc: string;

	videoPlayed: boolean = false;
	videoSrcSafe: SafeResourceUrl;
	
	
	
	constructor(
		private sanitizer: DomSanitizer
	) {}
	
	
	
	
	ngOnInit() {
		this.generateSafeVideoSource();
	}
	
	
	
	
	generateSafeVideoSource() : void {
		this.videoSrcSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.videoSrc);
	}




	generateYoutubeEmbedStyles() : any {
		return {
			'background-color': this.backgroundColor
		};
	}




	playVideo() : void {
		this.videoPlayed = true;
	}
	
	
	
	
	

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YteCustomOverlayComponent } from './yte-custom-overlay.component';

describe('YteCustomOverlayComponent', () => {
  let component: YteCustomOverlayComponent;
  let fixture: ComponentFixture<YteCustomOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YteCustomOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YteCustomOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

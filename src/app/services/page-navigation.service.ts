import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import { NavigationItem } from 'src/app/models/navigation-item';

@Injectable({
	providedIn: 'root'
})
export class PageNavigationService {

	
	constructor(
		private router: Router,
		private scrollToService: ScrollToService
	) { }




	public navigateTo(navigationItem: NavigationItem) : void {
		if(navigationItem.navigationType == 'redirect') {
			this.redirectTo(navigationItem.targetRoute);
		}

		else if(navigationItem.navigationType == 'scroll') {
			this.scrollTo(navigationItem.targetDiv);
		}

		else if(navigationItem.navigationType == 'redirectScroll') {
			this.redirectScrollTo(navigationItem);
		}
	}




	private scrollTo(targetDiv: string) : void {
		let scrollToConfig: ScrollToConfigOptions = {
			target: targetDiv,
			duration: 0,
			offset: -25
		};

		this.scrollToService.scrollTo(scrollToConfig);
	}




	private redirectTo(targetRoute: string) : void {
		this.router.navigateByUrl(targetRoute);
	}




	// redirect to a new route and then perform "scroll to element"
	// both targetRoute and targetDiv should have value
	private redirectScrollTo(navigationItem: NavigationItem) : void {
		this.router.navigateByUrl(navigationItem.targetRoute).then(() => {
			setTimeout(() => {
				this.scrollTo(navigationItem.targetDiv);
			}, 500);
		});
	}


	
	
	
	
	
}

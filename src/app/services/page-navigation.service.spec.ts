import { TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { PageNavigationService } from './page-navigation.service';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';


describe('PageNavigationService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			RouterTestingModule
		],
		providers: [
			ScrollToService
		]
	}));

	it('should be created', () => {
		const service: PageNavigationService = TestBed.get(PageNavigationService);
		expect(service).toBeTruthy();
	});
});

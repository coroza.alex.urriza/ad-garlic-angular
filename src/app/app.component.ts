import { Component, OnInit } from '@angular/core';
import AOS from 'aos';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})


export class AppComponent {

	title = 'ad-garlic-angular';

	constructor() {}
	


	
	ngOnInit() {
		AOS.init({
			// delay: 350,
			once: true,
			// disable: true,
		});
	}

	
	
	
	
}



import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { AdgButtonModule } from 'src/app/shared-modules/ad-garlic-ui/ad-garlic-ui.module';

import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

import { JobsInternshipsPageComponent } from './jobs-internships-page.component';
import { MainNavbarComponent } from 'src/app/components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from 'src/app/components/sidebar-navigation/sidebar-navigation.component';
import { JobsInternshipsSectionComponent } from 'src/app/components/jobs-internships-section/jobs-internships-section.component';
import { MainFooterComponent } from 'src/app/components/main-footer/main-footer.component';
import { SectionBannerComponent } from 'src/app/components/section-banner/section-banner.component';




describe('JobsInternshipsPageComponent', () => {
	let component: JobsInternshipsPageComponent;
	let fixture: ComponentFixture<JobsInternshipsPageComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				JobsInternshipsPageComponent,
				MainNavbarComponent,
				SidebarNavigationComponent,
				JobsInternshipsSectionComponent,
				MainFooterComponent,
				SectionBannerComponent
			],
			imports: [
				RouterTestingModule,
				AdgButtonModule
			],
			providers: [
				ScrollToService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(JobsInternshipsPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
	selector: 'app-sample-page',
	templateUrl: './sample-page.component.html',
	styleUrls: ['./sample-page.component.scss']
})
export class SamplePageComponent implements OnInit {

	constructor(
		private http: HttpClient,
		private router: Router
	) { }


	
	
	ngOnInit() {
	}




	redirect() {
		this.router.navigateByUrl('sample-page-two');
	}
	

	
	
}

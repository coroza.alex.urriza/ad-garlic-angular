import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { YoutubeEmbedModule } from 'src/app/shared-modules/youtube-embed/youtube-embed.module';

import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

import { CaseStudyPageComponent } from './case-study-page.component';
import { MainNavbarComponent } from 'src/app/components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from 'src/app/components/sidebar-navigation/sidebar-navigation.component';
import { CaseStudySectionComponent } from 'src/app/components/case-study-section/case-study-section.component';
import { MainFooterComponent } from 'src/app/components/main-footer/main-footer.component';




describe('CaseStudyPageComponent', () => {
	let component: CaseStudyPageComponent;
	let fixture: ComponentFixture<CaseStudyPageComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				CaseStudyPageComponent,
				MainNavbarComponent,
				SidebarNavigationComponent,
				CaseStudySectionComponent,
				MainFooterComponent
			],
			imports: [
				RouterTestingModule,
				NgbModule,
				YoutubeEmbedModule
			],
			providers: [
				ScrollToService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CaseStudyPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

import { Component, OnInit } from '@angular/core';
import { PageNavigationService } from 'src/app/services/page-navigation.service';
import { NavigationItem } from 'src/app/models/navigation-item';



@Component({
	selector: 'app-case-study-page',
	templateUrl: './case-study-page.component.html',
	styleUrls: ['./case-study-page.component.scss']
})
export class CaseStudyPageComponent implements OnInit {

	displayedNavItemsIndex: number[]; /* set of ids for the navItems that should be dislayed inline with the navbar */
	isSidebarNavigationOpen: boolean;
	navigationItems: NavigationItem[] = [];
	
	
	
	constructor(
		private pageNavService: PageNavigationService
	) {}
	
	
	
	
	ngOnInit() {
		this.displayedNavItemsIndex = [1, 2, 3, 4, 5, 6, 7, 12, 11, 10];
		this.isSidebarNavigationOpen = false;
		this.navigationItems = [
			{ displayName: 'HOME', navigationType: 'redirectScroll', targetDiv: '#home-section', targetRoute: '' },
			{ displayName: 'ABOUT', navigationType: 'redirectScroll', targetDiv: '#intro-section', targetRoute: '' },
			{ displayName: 'PHILOSOPHY', navigationType: 'redirectScroll', targetDiv: '#plusitivity-section', targetRoute: '' },
			{ displayName: 'SERVICES', navigationType: 'redirectScroll', targetDiv: '#services-section', targetRoute: '' },
			{ displayName: 'WORK', navigationType: 'redirectScroll', targetDiv: '#work-section', targetRoute: '' },
			{ displayName: 'CLIENTS', navigationType: 'redirectScroll', targetDiv: '#clients-section', targetRoute: '' },
			{ displayName: 'ORIGINALS', navigationType: 'redirectScroll', targetDiv: '#originals-section', targetRoute: '' },
			{ displayName: 'PEOPLE', navigationType: 'redirectScroll', targetDiv: '#people-section', targetRoute: '' },
			{ displayName: 'CONTACT US', navigationType: 'redirectScroll', targetDiv: '#contact-section', targetRoute: '' },
			{ displayName: 'CASE STUDIES', navigationType: 'redirect', targetDiv: '', targetRoute: 'case-study' },
			{ displayName: 'JOIN US', navigationType: 'redirect', targetDiv: '', targetRoute: 'jobs-and-internships' },
			{ displayName: 'PARTNERS', navigationType: 'redirect', targetDiv: '', targetRoute: 'partners' },
			{ displayName: 'NEWS', navigationType: 'redirect', targetDiv: '', targetRoute: 'news' },
		];
	}

	

	
	toggleSidebarNavigation() {
		this.isSidebarNavigationOpen = !this.isSidebarNavigationOpen;
	}




	navigateTo(navigationItem: NavigationItem) : void {
		this.pageNavService.navigateTo(navigationItem);
	}
	
	
	
	
}

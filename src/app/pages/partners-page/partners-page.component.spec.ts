import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { NgxGalleryModule } from 'ngx-gallery';
import { YoutubeEmbedModule } from 'src/app/shared-modules/youtube-embed/youtube-embed.module';

import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

import { PartnersPageComponent } from './partners-page.component';
import { MainNavbarComponent } from 'src/app/components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from 'src/app/components/sidebar-navigation/sidebar-navigation.component';
import { PartnersSectionComponent } from 'src/app/components/partners-section/partners-section.component';
import { MainFooterComponent } from 'src/app/components/main-footer/main-footer.component';
import { SectionBannerComponent } from 'src/app/components/section-banner/section-banner.component';




describe('PartnersPageComponent', () => {
	let component: PartnersPageComponent;
	let fixture: ComponentFixture<PartnersPageComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				PartnersPageComponent,
				MainNavbarComponent,
				SidebarNavigationComponent,
				PartnersSectionComponent,
				MainFooterComponent,
				SectionBannerComponent
			],
			imports: [
				RouterTestingModule,
				NgxGalleryModule,
				YoutubeEmbedModule
			],
			providers: [
				ScrollToService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PartnersPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

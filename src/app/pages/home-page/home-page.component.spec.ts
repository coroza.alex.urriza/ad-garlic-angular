import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxGalleryModule } from 'ngx-gallery';
import { AdgButtonModule } from 'src/app/shared-modules/ad-garlic-ui/ad-garlic-ui.module';
import { NgxMasonryModule } from 'ngx-masonry';
import { YoutubeEmbedModule } from 'src/app/shared-modules/youtube-embed/youtube-embed.module';

import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

import { HomePageComponent } from './home-page.component';
import { MainNavbarComponent } from 'src/app/components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from 'src/app/components/sidebar-navigation/sidebar-navigation.component';
import { HomeSectionComponent } from 'src/app/components/home-section/home-section.component';
import { IntroSectionComponent } from 'src/app/components/intro-section/intro-section.component';
import { PlusitivitySectionComponent } from 'src/app/components/plusitivity-section/plusitivity-section.component';
import { ServicesSectionComponent } from 'src/app/components/services-section/services-section.component';
import { WorkSectionComponent } from 'src/app/components/work-section/work-section.component';
import { ClientsSectionComponent } from 'src/app/components/clients-section/clients-section.component';
import { OriginalsSectionComponent } from 'src/app/components/originals-section/originals-section.component';
import { ContactSectionComponent } from 'src/app/components/contact-section/contact-section.component';
import { MainFooterComponent } from 'src/app/components/main-footer/main-footer.component';
import { WorkTileComponent } from 'src/app/components/work-tile/work-tile.component';


describe('HomePageComponent', () => {
	let component: HomePageComponent;
	let fixture: ComponentFixture<HomePageComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				HomePageComponent,
				MainNavbarComponent,
				SidebarNavigationComponent,
				HomeSectionComponent,
				IntroSectionComponent,
				PlusitivitySectionComponent,
				ServicesSectionComponent,
				WorkSectionComponent,
				ClientsSectionComponent,
				OriginalsSectionComponent,
				ContactSectionComponent,
				MainFooterComponent,
				WorkTileComponent
			],
			imports: [
				RouterTestingModule,
				NgbModule,
				NgxGalleryModule,
				NgxMasonryModule,
				YoutubeEmbedModule,
				AdgButtonModule
			],
			providers: [
				ScrollToService
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HomePageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

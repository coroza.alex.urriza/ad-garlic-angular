import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

import { NewsPageComponent } from './news-page.component';
import { MainNavbarComponent } from 'src/app/components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from 'src/app/components/sidebar-navigation/sidebar-navigation.component';
import { NewsSectionComponent } from 'src/app/components/news-section/news-section.component';
import { MainFooterComponent } from 'src/app/components/main-footer/main-footer.component';
import { SectionBannerComponent } from 'src/app/components/section-banner/section-banner.component';
import { NewsSummaryComponent } from 'src/app/components/news-summary/news-summary.component';




describe('NewsPageComponent', () => {
	let component: NewsPageComponent;
	let fixture: ComponentFixture<NewsPageComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				NewsPageComponent,
				MainNavbarComponent,
				SidebarNavigationComponent,
				NewsSectionComponent,
				MainFooterComponent,
				SectionBannerComponent,
				NewsSummaryComponent
			],
			imports: [
				RouterTestingModule
			],
			providers: [
				ScrollToService
			]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(NewsPageComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});

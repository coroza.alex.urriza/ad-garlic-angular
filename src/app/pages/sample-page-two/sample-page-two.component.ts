import { Component, OnInit } from '@angular/core';
import { Router }  from '@angular/router';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';




@Component({
	selector: 'app-sample-page-two',
	templateUrl: './sample-page-two.component.html',
	styleUrls: ['./sample-page-two.component.scss']
})
export class SamplePageTwoComponent implements OnInit {

	constructor(
		private router: Router,
		private scrollToService: ScrollToService
	) { }


	
	
	ngOnInit() {
	}




	goBack() {
		this.router.navigateByUrl('sample-page').then(() => {
			let scrollToConfig: ScrollToConfigOptions = {
				target: '#div10'
			};
	
			this.scrollToService.scrollTo(scrollToConfig);
		});	
	}
	
	

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMasonryModule } from 'ngx-masonry';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { NgxGalleryModule } from 'ngx-gallery';
import { EllipsisModule } from 'ngx-ellipsis';
import { InViewportModule } from 'ng-in-viewport';
import { HideableHeaderModule } from 'ngx-hideable-header';

import 'hammerjs';

// imports from shared-modules
import { AdGarlicUiModule, AdgButtonModule } from './shared-modules/ad-garlic-ui/ad-garlic-ui.module';
import { YoutubeEmbedModule } from './shared-modules/youtube-embed/youtube-embed.module';

import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MainNavbarComponent } from './components/main-navbar/main-navbar.component';
import { SidebarNavigationComponent } from './components/sidebar-navigation/sidebar-navigation.component';
import { HomeSectionComponent } from './components/home-section/home-section.component';
import { IntroSectionComponent } from './components/intro-section/intro-section.component';
import { ServicesSectionComponent } from './components/services-section/services-section.component';
import { MainFooterComponent } from './components/main-footer/main-footer.component';
import { PlusitivitySectionComponent } from './components/plusitivity-section/plusitivity-section.component';
import { WorkSectionComponent } from './components/work-section/work-section.component';
import { CaseStudySectionComponent } from './components/case-study-section/case-study-section.component';
import { ClientsSectionComponent } from './components/clients-section/clients-section.component';
import { ContactSectionComponent } from './components/contact-section/contact-section.component';
import { SamplePageComponent } from './pages/sample-page/sample-page.component';
import { CaseStudyPageComponent } from './pages/case-study-page/case-study-page.component';
import { JobsInternshipsSectionComponent } from './components/jobs-internships-section/jobs-internships-section.component';
import { JobsInternshipsPageComponent } from './pages/jobs-internships-page/jobs-internships-page.component';
import { NewsSectionComponent } from './components/news-section/news-section.component';
import { NewsPageComponent } from './pages/news-page/news-page.component';
import { NewsSummaryComponent } from './components/news-summary/news-summary.component';
import { SectionBannerComponent } from './components/section-banner/section-banner.component';
import { SamplePageTwoComponent } from './pages/sample-page-two/sample-page-two.component';
import { WorkTileComponent } from './components/work-tile/work-tile.component';
import { PartnersPageComponent } from './pages/partners-page/partners-page.component';
import { PartnersSectionComponent } from './components/partners-section/partners-section.component';
import { OriginalsSectionComponent } from './components/originals-section/originals-section.component';
import { GalleryItemPreviewModalComponent } from './components/gallery-item-preview-modal/gallery-item-preview-modal.component';
import { ChargedPeepsSectionComponent } from './components/charged-peeps-section/charged-peeps-section.component';


@NgModule({
	declarations: [
		AppComponent,
		HomePageComponent,
		MainNavbarComponent,
		SidebarNavigationComponent,
		HomeSectionComponent,
		IntroSectionComponent,
		ServicesSectionComponent,
		MainFooterComponent,
		PlusitivitySectionComponent,
		WorkSectionComponent,
		CaseStudySectionComponent,
		ClientsSectionComponent,
		ContactSectionComponent,
		SamplePageComponent,
		CaseStudyPageComponent,
		JobsInternshipsSectionComponent,
		JobsInternshipsPageComponent,
		NewsSectionComponent,
		NewsPageComponent,
		NewsSummaryComponent,
		SectionBannerComponent,
		SamplePageTwoComponent,
		WorkTileComponent,
		PartnersPageComponent,
		PartnersSectionComponent,
		OriginalsSectionComponent,
		GalleryItemPreviewModalComponent,
		ChargedPeepsSectionComponent
	],
	entryComponents: [
		GalleryItemPreviewModalComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		FlexLayoutModule,
		NgbModule,
		NgxMasonryModule,
		ScrollToModule.forRoot(),
		NgxGalleryModule,
		EllipsisModule,
		InViewportModule,
		HideableHeaderModule,
		AdGarlicUiModule, AdgButtonModule,
		YoutubeEmbedModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }

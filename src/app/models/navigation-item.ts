export interface NavigationItem {
    id?: number;
    displayName: string;
    icon?: string;
    navigationType: 'redirect'|'scroll'|'redirectScroll'; /* redirectScroll means it will redirect to a new route then perform "scroll to element", both targetRoute and targetDiv should have value */
    targetDiv: string; /* ID selector, used if navigationType = scroll */
    targetRoute: string; /* Angular route string, used if navigationType = redirect */
}
export interface CareerOpening {
    id?: number;
    title: string;
    description: string;
    openingType: string; /* job or internship */
}
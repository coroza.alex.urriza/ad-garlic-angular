export interface News {
    id?: number;
    title: string;
    bannerImageUrl: string; 
    textContent: string;
    otherImageUrls?: string[];
    tags: string[];
}
export interface Project {
    id?: number;
    title: string;
    imageUrl: string;
}
export interface AdGarlicService {
    id?: number;
    name: string;
    fullName: string;
    icon: string;
    description: string;
    galleryItems: string[]
}
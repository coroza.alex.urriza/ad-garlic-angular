export interface Client {
    id?: number;
    name: string;
    logoUrl: string;
}
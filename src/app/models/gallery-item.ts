import { SafeResourceUrl } from '@angular/platform-browser';

export interface GalleryItem {
    id?: number;
    name?: string,
    itemType: 'image' | 'video';
    sourceUrl: string | SafeResourceUrl;
    caption?: string;
}